﻿namespace Game_MVC.Infrastructure.Data
{
    using Game_MVC.Infrastructure.Business.Stores;

    public class InMemoryStore<TEntity> : IStore<TEntity>
        where TEntity : new()
    {
        private TEntity variable;

        public TEntity Load()
        {
            this.variable = new TEntity();
            return this.variable;
        }

        public void Save()
        {
        }
    }
}