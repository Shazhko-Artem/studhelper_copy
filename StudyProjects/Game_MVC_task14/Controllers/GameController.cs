﻿using System;
using System.Web.Mvc;
using Game_MVC.Services.Interfaces;
using Game_MVC.Services.Interfaces.Projections;
using Game_MVC_task14.Hubs;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace Game_MVC_task14.Controllers
{
    [RoutePrefix("game")]
    [System.Web.Mvc.Authorize]
    public class GameController : Controller
    {
        private readonly IGameService gameService;
        private readonly IHubContext gameHubContext;
        private readonly IUserService userService;

        public GameController(IGameService gameService, IUserService userService)
        {
            this.gameService = gameService;
            this.userService = userService;
            this.gameHubContext = GlobalHost.ConnectionManager.GetHubContext<GameHub>();
        }

        public ActionResult Index()
        {
            return this.View();
        }

        [Route("history")]
        public ActionResult GameHistory()
        {
            var result = this.gameService.GetGameHistory();
            if (result.State.IsFail())
            {
                return this.HttpNotFound();
            }

            return this.View(result.Result);
        }

        [Route("history/details/{id}")]
        public ActionResult GameDetails(Guid id)
        {
            var result = this.gameService.GetGameDetails(id);
            if (result.State.IsFail())
            {
                return this.HttpNotFound();
            }

            return this.View(result.Result);
        }

        [HttpPost]
        [Route("compare/{number}")]
        public ActionResult CompareNumber(int number)
        {
            UserProjection user = this.GetCurrentUser();

            var compareNumberResult = this.gameService.CompareNumber(user, number);

            if (compareNumberResult.State.IsFail())
            {
                return this.Json(new { result = "error", message = "Something went wrong" });
            }

            switch (compareNumberResult.Result)
            {
                case 1:
                    return this.Json(new { result = "loss", message = "Number is less" });
                case -1:
                    return this.Json(new { result = "loss", message = "Number is more" });
                case 0:
                    this.gameHubContext.Clients.All.onGameOver(user.Name, $"The user '{user.Name}' win. Number: {number}");
                    return this.Json(new { result = "win", message = "You win!!!" });
            }

            return this.Json(new { result = "error", message = "Something went wrong" });
        }

        [HttpPost]
        [Route("set/{number}")]
        public void SetNumber(int number)
        {
            UserProjection user = this.GetCurrentUser();

            if (this.gameService.SetNumber(user, number).State.IsSuccess())
            {
                this.gameHubContext.Clients.All.onStartGame();
            }
        }

        [HttpPost]
        [Route("connect")]
        public ActionResult Connect()
        {
            string userName = this.User.Identity.Name;

            if (this.gameService.IsRunning)
            {
                return this.Json(new { userName, gameStatus = "running" });
            }

            return this.Json(new { userName, gameStatus = "stopped" });
        }

        [HttpPost]
        [Route("markYourSelf")]
        public void MarkYourSelf()
        {
            string userName = this.User.Identity.Name;
            this.gameHubContext.Clients.All.onMarkedOtherUsers(userName);
        }

        [HttpPost]
        [Route("rollColl")]
        public void RollColl()
        {
            string userName = this.User.Identity.Name;
            this.gameHubContext.Clients.All.onRollColl(userName);
        }

        private UserProjection GetCurrentUser()
        {
            Guid userId = Guid.Parse(this.User.Identity.GetUserId());
            var searchOperationResult = this.userService.GetFirst(u => u.Id == userId);
            if (searchOperationResult.State.IsFail())
            {
                return null;
            }

            return searchOperationResult.Result;
        }
    }
}