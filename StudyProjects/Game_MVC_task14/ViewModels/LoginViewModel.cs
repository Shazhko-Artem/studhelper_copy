﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Game_MVC_task14.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [MinLength(2)]
        [DisplayName("User name")]
        [RegularExpression("[a-zA-Z0-9_\\-]+")]
        public string UserName { get; set; }

        [Required]
        [MinLength(3)]
        [DisplayName("Password")]
        [RegularExpression("[a-zA-Z0-9_\\-]+")]
        public string Password { get; set; }
    }
}