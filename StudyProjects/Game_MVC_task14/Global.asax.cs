﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Game_MVC_task14.App_Start;

namespace Game_MVC_task14
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}