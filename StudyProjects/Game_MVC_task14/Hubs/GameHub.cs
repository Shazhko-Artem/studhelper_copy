﻿using Microsoft.AspNet.SignalR;

namespace Game_MVC_task14.Hubs
{
    [Authorize]
    public class GameHub : Hub
    {
        public GameHub()
        {
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            string userName = this.Context.User.Identity.Name;
            this.Clients.All.onUserDisconnected(userName);
            return base.OnDisconnected(stopCalled);
        }
    }
}