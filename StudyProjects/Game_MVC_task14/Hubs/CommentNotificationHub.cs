﻿using Microsoft.AspNet.SignalR;

namespace Game_MVC_task14.Hubs
{
    public class CommentNotificationHub : Hub
    {
        public void Connect()
        {
            string userName = this.Context.User.Identity.Name;
            this.Clients.Caller.onConnected(userName);
        }
    }
}