﻿using System;
using SimpleInjector;

namespace Game_MVC_task14.Infrastructure
{
    public static class SimpleInjectorExtensions
    {
        public static void RegisterFuncFactory<TService, TImpl>(
            this Container container, Lifestyle lifestyle = null)
            where TService : class
            where TImpl : class, TService
        {
            lifestyle = lifestyle ?? container.Options.DefaultLifestyle;
            var producer = lifestyle.CreateProducer<TService, TImpl>(container);
            container.RegisterInstance<Func<TService>>(producer.GetInstance);
        }
    }
}