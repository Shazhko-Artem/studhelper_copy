﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamInputOutput_tast01
{
    class Program
    {
        private static string filePath = "userStrings.txt";

        static void Main(string[] args)
        {
            MemoryStream memoryStream = new MemoryStream();
            Console.WriteLine("==STRING==");
            InputUserString(memoryStream);
            memoryStream.Seek(0, SeekOrigin.Begin);
            Console.WriteLine("===SAVE===");
            SaveToFile(memoryStream);
            ShowFileContent();

            Console.ReadKey();
        }

        static void InputUserString(Stream stream)
        {
            StreamWriter writer = new StreamWriter(stream,Encoding.UTF8);
            for (int i = 0; i < 10; i++)
            {
                writer.WriteLine(Console.ReadLine());
            }

            writer.Flush();
        }

        static void SaveToFile(Stream stream)
        {
            using (StreamWriter writer = new StreamWriter(new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write)))
            {
                using(StreamReader reader=new StreamReader(stream))
                {
                    while (!reader.EndOfStream)
                    {
                        writer.WriteLine(reader.ReadLine());
                    }
                }
            }
        }

        static void ShowFileContent()
        {
            using(StreamReader reader = new StreamReader(new FileStream(filePath, FileMode.Open, FileAccess.Read)))
            {
                while (!reader.EndOfStream)
                {
                    Console.WriteLine(reader.ReadLine());
                }
            }
        }
    }
}