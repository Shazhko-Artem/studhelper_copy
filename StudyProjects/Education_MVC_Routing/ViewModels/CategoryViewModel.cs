﻿namespace Education_MVC_Routing.ViewModels
{
    public class CategoryViewModel
    {
        public string CategoryName { get; set; }

        public string SubCategoryName { get; set; }

        public CategoryViewModel() { }

        public CategoryViewModel(string categoryName):this(categoryName,null)
        {

        }

        public CategoryViewModel(string categoryName, string subCategoryName)
        {
            this.CategoryName = categoryName;
            this.SubCategoryName = subCategoryName;
        }
    }
}