﻿namespace Game_MVC.Domain.Interfaces
{
    using System;
    using System.Collections.Generic;

    public interface IRepository<TEntity>
    {
        TEntity Add(TEntity entity);

        IList<TEntity> Get(
            Func<TEntity, bool> filter = null,
            string includeProperties = "");

        TEntity GetFirst(
            Func<TEntity, bool> filter = null,
            string includeProperties = "");

        TEntity GetLast(
            Func<TEntity, bool> filter = null,
            string includeProperties = "");

        TEntity Update(TEntity entity);

        bool Remove(TEntity entity);

        bool SaveChanges();
    }
}