﻿namespace Game_MVC.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Game_MVC.Domain.Core;
    using Game_MVC.Domain.Interfaces;

    public interface ICommentService
    {
        OperationResult<Comment> Add(string userName, string message);

        OperationResult<IList<Comment>> Get(Func<Comment, bool> filter = null);

        OperationResult<Comment> GetFirst(Func<Comment, bool> filter);

        OperationResult<Comment> Update(string userName, Guid commentId, string message);

        OperationResult Remove(string userName, Guid commentId);
    }
}