﻿using Game_MVC.Services.Interfaces.Projections;

namespace Game_MVC.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Game_MVC.Domain.Core;

    public interface IUserService
    {
        OperationResult<UserProjection> Add(User user);

        OperationResult<UserProjection> Find(string name, string password);

        OperationResult<IList<UserProjection>> Get(Func<User, bool> filter = null);

        OperationResult<UserProjection> GetFirst(Func<User, bool> filter);

        OperationResult<UserProjection> Update(User user);
    }
}