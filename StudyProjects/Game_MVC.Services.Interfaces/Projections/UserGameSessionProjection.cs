﻿using System;

namespace Game_MVC.Services.Interfaces.Projections
{
    public class UserGameSessionProjection
    {
        public Guid Id { get; set; }

        public UserProjection User { get; set; }

        public GameProjection Game { get; set; }

        public int Number { get; set; }

        public DateTime DateTime { get; set; }
    }
}