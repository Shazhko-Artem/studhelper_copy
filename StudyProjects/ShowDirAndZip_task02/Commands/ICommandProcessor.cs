﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowDirAndZip_task02.Commands
{
    public interface ICommandProcessor
    {
        void Process(int command);

        IEnumerable<ICommand> Commands { get; }
    }
}
