﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowDirAndZip_task02.Commands
{
    public class CommandManager
    {
        private readonly ICommandProcessor processor;
        private string info;

        public CommandManager(ICommandProcessor commandProcessor)
        {
            this.processor = commandProcessor;
        }

        public void Start()
        {
            this.SetupInfo();

            while (true)
            {
                Console.Write(this.info);

                var input = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input)
                    ||
                    !int.TryParse(input, out var command))
                {
                    continue;
                }

                this.processor.Process(command);
                Console.ReadLine();
            }
        }

        private void SetupInfo()
        {
            var sb = new StringBuilder();
            var commands = this.processor.Commands;

            sb.AppendLine("Select operation:");

            foreach (var command in commands)
            {
                sb.AppendLine($"\t{command.Number}. {command.DisplayName}");
            }

            sb.Append("[MENU] --> ");
            this.info = sb.ToString();
        }
    }
}
