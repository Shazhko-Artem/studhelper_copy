﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowDirAndZip_task02.Commands
{
    public class CommandProcessor : ICommandProcessor
    {
        private readonly Dictionary<int, ICommand> commands = new Dictionary<int, ICommand>();

        public CommandProcessor(IEnumerable<ICommand> commands)
        {
            foreach (var command in commands)
            {
                if (!this.commands.ContainsKey(command.Number))
                    this.commands.Add(command.Number, command);
            }
        }

        public void Process(int number)
        {
            if (!this.commands.TryGetValue(number, out var command)) return;

            command.Execute();
        }

        public IEnumerable<ICommand> Commands => this.commands.Values.AsEnumerable();
    }
}
