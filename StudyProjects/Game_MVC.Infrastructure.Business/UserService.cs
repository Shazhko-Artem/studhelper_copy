﻿namespace Game_MVC.Infrastructure.Business
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using AutoMapper;
    using Game_MVC.Domain.Core;
    using Game_MVC.Domain.Interfaces;
    using Game_MVC.Services.Interfaces;
    using Game_MVC.Services.Interfaces.Projections;
    using Newtonsoft.Json;

    public class UserService : IUserService
    {
        private readonly IRepository<User> repository;
        private readonly IMapper mapper;
        private readonly IPasswordHasher passwordHasher;

        public UserService(IRepository<User> repository, IPasswordHasher passwordHasher)
        {
            this.repository = repository;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserProjection>();
                cfg.CreateMap<UserProjection, User>();
            });
            this.passwordHasher = passwordHasher;
            this.mapper = config.CreateMapper();
        }

        public OperationResult<UserProjection> Add(User user)
        {
            var checkOnExistResult = this.repository.GetFirst(u => u.Name == user.Name);
            if (checkOnExistResult != null)
            {
                return new OperationResult<UserProjection>()
                {
                    Message = "User with the same name already exists",
                    Code = ErrorCode.AlreadyExist
                };
            }

            user.Password = this.passwordHasher.HashPassword(user.Password);
            try
            {
                var result = this.repository.Add(user);
                if (!this.repository.SaveChanges())
                {
                    return new OperationResult<UserProjection>()
                    {
                        Code = ErrorCode.Other
                    };
                }

                return new OperationResult<UserProjection>() { Result = this.mapper.Map<User, UserProjection>(result) };
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"[{DateTime.Now}] Exception: {exception.Message}{Environment.NewLine}When add new user: {JsonConvert.SerializeObject(user)}");
                return new OperationResult<UserProjection>()
                {
                    Message = exception.Message, Code = ErrorCode.Other
                };
            }
        }

        public OperationResult<UserProjection> Find(string name, string password)
        {
            var foundUser = this.repository.GetFirst(user => user.Name == name);
            if (foundUser == null)
            {
                return new OperationResult<UserProjection>() { Code = ErrorCode.NotFound };
            }

            if (this.passwordHasher.VerifyHashedPassword(foundUser.Password, password))
            {
                return new OperationResult<UserProjection>()
                {
                    Result = this.mapper.Map<User, UserProjection>(foundUser)
                };
            }

            return new OperationResult<UserProjection>()
            {
                Code = ErrorCode.Forbidden
            };
        }

        public OperationResult<IList<UserProjection>> Get(Func<User, bool> filter = null)
        {
            var result = this.repository.Get(filter);
            if (result == null)
            {
                return new OperationResult<IList<UserProjection>>() { Code = ErrorCode.NotFound };
            }

            return new OperationResult<IList<UserProjection>>()
            {
                Result = this.mapper.Map<IList<User>, IList<UserProjection>>(result)
            };
        }

        public OperationResult<UserProjection> GetFirst(Func<User, bool> filter)
        {
            var result = this.repository.GetFirst(filter);
            if (result == null)
            {
                return new OperationResult<UserProjection>() { Code = ErrorCode.NotFound };
            }

            return new OperationResult<UserProjection>() { Result = this.mapper.Map<User, UserProjection>(result) };
        }

        public OperationResult<UserProjection> Update(User user)
        {
            var foundUser = this.repository.GetFirst(u => u.Id == user.Id);
            if (foundUser == null)
            {
                return new OperationResult<UserProjection>() { Code = ErrorCode.NotFound };
            }

            if (user.Password != foundUser.Password)
            {
                var hashedPassword = this.passwordHasher.HashPassword(user.Password);
                if (hashedPassword != foundUser.Password)
                {
                    user.Password = hashedPassword;
                }
            }

            var result = this.repository.Update(user);
            if (!this.repository.SaveChanges())
            {
                return new OperationResult<UserProjection>()
                {
                    Code = ErrorCode.Other
                };
            }

            return new OperationResult<UserProjection>() { Result = this.mapper.Map<User, UserProjection>(result) };
        }
    }
}