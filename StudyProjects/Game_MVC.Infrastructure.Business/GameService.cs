﻿namespace Game_MVC.Infrastructure.Business
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Game_MVC.Domain.Core;
    using Game_MVC.Domain.Interfaces;
    using Game_MVC.Services.Interfaces;
    using Game_MVC.Services.Interfaces.Projections;

    public sealed class GameService : IGameService
    {
        private readonly IRepository<UserGameSession> gameSessionRepository;
        private readonly IRepository<Game> gameRepository;
        private readonly object monitor = new object();
        private readonly IMapper mapper;
        private Game currentGame;

        public GameService(IRepository<Game> gameRepository, IRepository<UserGameSession> gameSessionRepository)
        {
            this.gameRepository = gameRepository;
            this.gameSessionRepository = gameSessionRepository;
            this.LoadCurrentGame();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Game, GameProjection>();
                cfg.CreateMap<GameProjection, Game>();
            });

            this.mapper = config.CreateMapper();
        }

        public bool IsRunning => this.currentGame != null;

        public OperationResult SetNumber(UserProjection user, int number)
        {
            lock (this.monitor)
            {
                var game = new Game() { OwnerId = user.Id, StartTime = DateTime.Now, Number = number };

                if (this.IsRunning)
                {
                    this.currentGame.EndTime = DateTime.Now;
                }

                this.gameRepository.Add(game);
                this.currentGame = game;
                if (!this.gameRepository.SaveChanges())
                {
                    return new OperationResult() { Code = ErrorCode.Other, Message = "Failed to save data" };
                }
            }

            return new OperationResult();
        }

        public OperationResult<int> CompareNumber(UserProjection user, int number)
        {
            Game oldGame = null;
            int result;
            lock (this.monitor)
            {
                if (!this.IsRunning)
                {
                    return new OperationResult<int>() { Message = "The game is not running", Code = ErrorCode.Other };
                }

                result = number.CompareTo(this.currentGame.Number);
                this.currentGame.GameSessions.Add(new UserGameSession() { UserId = user.Id, DateTime = DateTime.Now, Number = number });
                if (result == 0)
                {
                    oldGame = this.currentGame;
                    oldGame.EndTime = DateTime.Now;
                    oldGame.WinnerId = user.Id;
                    this.currentGame = null;
                }
            }

            if (oldGame != null)
            {
                this.gameRepository.Update(oldGame);
            }

            this.gameRepository.SaveChanges();
            return new OperationResult<int>() { Result = result };
        }

        public OperationResult<IList<GameProjection>> GetGameHistory(Func<Game, bool> filter = null)
        {
            var result = this.gameRepository.Get(filter, $"{nameof(Game.Owner)},{nameof(Game.Winner)},{nameof(Game.GameSessions)},{nameof(Game.GameSessions)}.{nameof(UserGameSession.User)}");
            return new OperationResult<IList<GameProjection>>() { Result = this.mapper.Map<IList<Game>, IList<GameProjection>>(result) };
        }

        public OperationResult<IList<UserGameSessionProjection>> GetGameDetails(GameProjection game)
        {
            return this.GetGameDetails(game.Id);
        }

        public OperationResult<IList<UserGameSessionProjection>> GetGameDetails(Guid gameId)
        {
            var result = this.gameSessionRepository.Get(session => session.GameId == gameId,
                $"{nameof(UserGameSessionProjection.User)},{nameof(UserGameSessionProjection.Game)}");
            return new OperationResult<IList<UserGameSessionProjection>>() { Result = this.mapper.Map<IList<UserGameSession>, IList<UserGameSessionProjection>>(result) };
        }

        private void LoadCurrentGame()
        {
            var previousSession = this.gameRepository.GetLast();
            if (previousSession?.WinnerId == null)
            {
                this.currentGame = previousSession;
            }
        }
    }
}