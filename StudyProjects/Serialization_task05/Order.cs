﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Serialization_task05
{
    [Serializable]
    public class Order : IXmlSerializable
    {
        [NonSerialized]
        private decimal totalPrice;

        public int Count { get; set; }

        public decimal Price { get; set; }

        public decimal TotalPrice
        {
            get
            {
                return this.totalPrice;
            }

            set
            {
                this.totalPrice = value;
            }
        }

        public Order(int count, decimal price)
        {
            this.Count = count;
            this.Price = price;
        }

        protected Order()
        {
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            this.Price = decimal.Parse(reader.GetAttribute("Price"));
            this.Count = int.Parse(reader.GetAttribute("Count"));
            this.TotalPrice = this.Price * this.Count;
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Price", this.Price.ToString());
            writer.WriteAttributeString("Count", this.Count.ToString());
        }
    }
}