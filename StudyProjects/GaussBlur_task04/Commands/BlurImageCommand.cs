﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using GaussBlur_task04.Business;
using GaussBlur_task04.Helpers;
using GaussBlur_task04.ViewModels;

namespace GaussBlur_task04.Commands
{
    public class BlurImageCommand : ICommand
    {
        private MainViewModel viewModel;

        public BlurImageCommand(MainViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.viewModel.PropertyChanged += this.OnViewModelChangedState;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return this.viewModel.ImagePixels != null && this.viewModel.OriginBitmapImage != null;
        }

        public async void Execute(object parameter)
        {
            if (this.viewModel.OriginBitmapImage == null || this.viewModel.ImagePixels == null)
            {
                return;
            }

            this.viewModel.IsNotRunProcessing = false;
            int width = this.viewModel.OriginBitmapImage.PixelWidth;
            int height = this.viewModel.OriginBitmapImage.PixelHeight;
            GaussBlur blurImage = new GaussBlur(this.viewModel.ImagePixels, width, height);
            long start = DateTime.Now.Ticks;
            await blurImage.BlurImage(this.viewModel.IsParallel);
            long end = DateTime.Now.Ticks;
            this.viewModel.SpeedTime = end - start;
            this.viewModel.BlurredBitmapImage = this.viewModel.ImagePixels.ToBlackAndWhite(width, height).ToImageSource(width, height);
            this.viewModel.IsNotRunProcessing = true;
        }

#pragma warning disable Nix09 // The parameter is never used
        private void OnViewModelChangedState(object sender, PropertyChangedEventArgs e)
#pragma warning restore Nix09 // The parameter is never used
        {
            if (e.PropertyName == nameof(this.viewModel.OriginBitmapImage))
            {
                this.CanExecuteChanged?.Invoke(this, e);
            }
        }
    }
}