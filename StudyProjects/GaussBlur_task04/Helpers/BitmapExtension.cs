﻿using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace GaussBlur_task04.Helpers
{
    public static class BitmapExtension
    {
        public static BitmapImage ToImageSource(this Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public static BitmapImage ToImageSource(this Color[][] pixels, int width, int height)
        {
            Bitmap bitmap = new Bitmap(width, height);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    bitmap.SetPixel(x, y, pixels[y][x]);
                }
            }

            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public static Color[][] ToBlackAndWhite(this Color[][] pixels, int width, int height)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Color pixel = pixels[x][y];
                    int avg = (pixel.R + pixel.G + pixel.B) / 3;

                    pixels[x][y] = Color.FromArgb(0, avg, avg, avg);
                }
            }

            return pixels;
        }

        public static Bitmap ToBlackAndWhite(this Bitmap image)
        {
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    Color pixel = image.GetPixel(x, y);
                    int avg = (pixel.R + pixel.G + pixel.B) / 3;

                    image.SetPixel(x, y, Color.FromArgb(0, avg, avg, avg));
                }
            }

            return image;
        }

        public static Color[][] ToPixels(this Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;
            Color[][] pixels = new Color[height][];
            for (int y = 0; y < height; y++)
            {
                pixels[y] = new Color[width];
                for (int x = 0; x < width; x++)
                {
                    pixels[y][x] = image.GetPixel(x, y);
                }
            }

            return pixels;
        }
    }
}