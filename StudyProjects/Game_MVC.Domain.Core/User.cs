﻿namespace Game_MVC.Domain.Core
{
    using System;
    using System.Collections.Generic;

    public class User : IUnique
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public IList<UserGameSession> GameSessions { get; set; } = new List<UserGameSession>();

        public IList<Game> Owners { get; set; } = new List<Game>();

        public IList<Game> WonGames { get; set; } = new List<Game>();
    }
}