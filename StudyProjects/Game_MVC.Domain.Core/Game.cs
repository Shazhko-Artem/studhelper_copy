﻿namespace Game_MVC.Domain.Core
{
    using System;
    using System.Collections.Generic;

    public class Game : IUnique
    {
        public Guid Id { get; set; }

        public Guid? OwnerId { get; set; }

        public User Owner { get; set; }

        public int Number { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public Guid? WinnerId { get; set; }

        public User Winner { get; set; }

        public IList<UserGameSession> GameSessions { get; set; } = new List<UserGameSession>();
    }
}