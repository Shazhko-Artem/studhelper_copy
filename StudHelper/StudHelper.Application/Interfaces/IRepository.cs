﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace StudHelper.Application.Interfaces
{
    public interface IRepository<TEntity>
    {
        TEntity Add(TEntity entity);

        IList<TEntity> Get<TProperty>(
            Func<TEntity, bool> filter = null,
            params Expression<Func<TEntity, TProperty>>[] includeProperties);

        TEntity GetFirst<TProperty>(
            Func<TEntity, bool> filter = null,
            params Expression<Func<TEntity, TProperty>>[] includeProperties);

        TEntity GetLast<TProperty>(
            Func<TEntity, bool> filter = null,
            params Expression<Func<TEntity, TProperty>>[] includeProperties);

        TEntity Update(TEntity entity);

        bool Remove(TEntity entity);

        bool SaveChanges();
    }
}