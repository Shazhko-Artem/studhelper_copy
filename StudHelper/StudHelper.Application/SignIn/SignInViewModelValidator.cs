﻿using FluentValidation;

namespace StudHelper.Application.SignIn
{
    public class SignInViewModelValidator : AbstractValidator<SignInViewModel>
    {
        public SignInViewModelValidator()
        {
            // TODO: move all strings to resources
            this.RuleFor(x => x.Email).EmailAddress().WithMessage("Please enter a valid email");
            this.RuleFor(x => x.Password).NotEmpty();
        }
    }
}