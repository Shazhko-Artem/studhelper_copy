﻿using System.ComponentModel.DataAnnotations;

namespace StudHelper.Application.SignUp
{
    public class SignUpViewModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }
    }
}