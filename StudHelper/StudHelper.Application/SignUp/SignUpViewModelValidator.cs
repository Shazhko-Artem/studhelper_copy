﻿using FluentValidation;

namespace StudHelper.Application.SignUp
{
    public class SignUpViewModelValidator : AbstractValidator<SignUpViewModel>
    {
        public SignUpViewModelValidator()
        {
            // TODO: move all strings to resources
            this.RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("Please enter a valid email");
            this.RuleFor(x => x.Password).NotEmpty();
            this.RuleFor(x => x.ConfirmPassword).Equal(x => x.Password).WithName("Confirm password");
            this.RuleFor(x => x.FirstName).NotEmpty().WithName("First name");
            this.RuleFor(x => x.LastName).NotEmpty().WithName("Last name");
        }
    }
}