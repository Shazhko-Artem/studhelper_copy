﻿using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using StudHelper.Application.SignIn;
using StudHelper.Application.SignUp;
using StudHelper.Domain.Entities;
using StudHelper.Web.Services;

namespace StudHelper.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IEmailSender emailSender;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IEmailSender emailSender)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.emailSender = emailSender;
        }

        public IActionResult SignIn()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> SignIn(SignInViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var result = await this.signInManager.PasswordSignInAsync(model.Email, model.Password, model.StaySignedIn, false);
            if (result.Succeeded)
            {
                return this.RedirectToAction("Index", "Home");
            }
            else
            {
                this.ModelState.AddModelError(string.Empty, "Incorrect email or password");
                return this.View(model);
            }
        }

        public IActionResult SignUp()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> SignUp(SignUpViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            User newUser = new User() { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };

            var result = await this.userManager.CreateAsync(newUser, model.Password);

            if (result.Succeeded)
            {
                var code = await this.userManager.GenerateEmailConfirmationTokenAsync(newUser);
                var callbackUrl = Url.Page(
                    "/Account/ConfirmEmail",
                    pageHandler: null,
                    values: new { userId = newUser.Id, code = code },
                    protocol: Request.Scheme);

                await this.emailSender.SendEmailAsync(newUser.Email, "Confirm your email",
                    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");


                //await this.signInManager.SignInAsync(newUser, false);
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error.Description);
                }

                return this.View(model);
            }

            return this.RedirectToAction("Index", "Home");
        }
    }
}