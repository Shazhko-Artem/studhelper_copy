﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using StudHelper.Domain.Entities;

namespace StudHelper.Persistence
{

    public class StudHelperDbContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public StudHelperDbContext(DbContextOptions<StudHelperDbContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(StudHelperDbContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}